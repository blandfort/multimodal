# Multimodal Social Media Analysis for Gang Violence Prevention

This repository contains code to train and evaluate models for detecting the _psychosocial codes_ "aggression", "loss" and "substance use" from tweets with images.

Experiments can be run in the jupyter notebooks:

- `main_experiment.ipynb` contains the main experiment on psychosocial code detection, where various models are trained and evaluated, using various textual and visual features.
- `sensitivity_analysis.ipynb` runs an experiment for analyzing which local visual features are relied on most by the detector.
- `ablation_study.ipynb` is used to check which local visual features are necessary for detecting the different psychosocial codes (by removing individual features and testing whether this diminishes detection performance).

For running the experiments, some data and feature files are necessary which are _not_ included in this repository. (See below.)


## Data

The following files with tweets and annotations are needed:

- `fold_tweets.json` contains the assignment of tweets to folds (we use 5-fold cross validation for all experiments). The format is {"0": [tweet_ID1, tweet_ID2, ...], "1": ...}.
- `tweet_codes.json` contains psychosocial code annotations in the format {tweet_ID1: [annotation1, annotation2, ...], tweet_ID2: [annotation1, annotation2, ...], ...}, where each annotation is given as [aggression (binary), code (binary), substance use (binary), annotator].

NOTE: Tweet IDs and annotations for the data can be obtained by researchers who sign an MOU
specifying their intended use of the data and their agreement with our ethical guidelines.


## Features

Input features for our experiments are given as dictionaries with tweet IDs as keys and a list or 1D numpy array of values for each tweet.
We used the following features:

- Visual features:
  - Image embeddings computed with inception-v3 CNN.
  - Counts of local visual concepts, detected with Faster R-CNN. (For each detection, a confidence value is output. We only consider detections above a certain threshold, which is set to either 0.1 or 0.5.)
  - Counts of local visual concepts based on annotations. (Not used for the main experiment.)
- Textual features:
  - Text embeddings of a character-level CNN.
  - Text embeddings of a word-level CNN.
  - Linguistic features (uni-grams, emotion scores etc.)
- _Fusion features_ are combinations of the above.

See features.py for details on how feature files are read.

It is possible to use own features in the experiments by passing them as feature dictionary (see e.g. usage of inception features) or modifying the make_input_features function in features.py.


## Citation

This code repository was created for the paper "Multimodal Social Media Analysis for Gang Violence Prevention".
If any of the code was helpful for your research, please consider citing it:

    @article{blandfort2019multimodal,
      title={Multimodal Social Media Analysis for Gang Violence Prevention},
      author={Blandfort, Philipp and Patton, Desmond and Frey, William R and Karaman, Svebor and Bhargava, Surabhi and Lee, Fei-Tzin and Varia, Siddharth and Kedzie, Chris and Gaskell, Michael B and Schifanella, Rossano and McKeown, Kathleen and Chang, Shih-Fu},
      journal={13th International AAAI Conference on Web and Social Media (ICWSM-19)},
      year={2019}
    }

Our paper also contains further information about the approach and overall context.

