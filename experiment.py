import numpy as np

from models import svm_classification
from data import make_regression_data, CODES, fold_tweets, train_test_tweets
from results import write_result_dict, result_dict_from_file, merge_result_dicts
from evaluation import compute_performance


def run_evaluation(train_data, eval_data, model=svm_classification, threshold=0, verbose=True):
    if verbose:
        print("Running evaluation for threshold %0.2f ..."%threshold)
        print("Training data: %d positive samples, %d negative samples"%(sum([d[1]>threshold for d in train_data]),
                                                                         sum([d[1]<=threshold for d in train_data])))
        print("Test data: %d positive samples, %d negative samples"%(sum([d[1]>threshold for d in eval_data]),
                                                                     sum([d[1]<=threshold for d in eval_data])))
    
    X_train = np.array([d[0] for d in train_data])
    y_train = 1*np.array([d[1]>threshold for d in train_data])
    X_eval = np.array([d[0] for d in eval_data])
    y_eval = 1*np.array([d[1]>threshold for d in eval_data])
    
    predictions, prediction_scores = model(X_train, y_train, X_eval)
    performance, performance_header, n_stuff = compute_performance(predictions, prediction_scores, y_eval)

    if verbose:
        print('*', ' '.join(['%s %0.2f'%(performance_header[i], num) for i,num in enumerate(performance)]))
    
    return performance, performance_header, n_stuff+tuple([len(eval_data)])



def run_experiment(model, name, folds=fold_tweets.keys(), threshold=0., verbose=False,
                   feature_dict=None, feature_type=None, codes=CODES, **kwargs):
    results = np.zeros((len(codes), 1, len(folds), 5+3))

    for c,code in enumerate(codes):
        if verbose:
            print('\n',code)
        
        for i,fold in enumerate(folds):
            train_ids, test_ids = train_test_tweets(fold)

            scores,header,n_stuff = run_evaluation(train_data=make_regression_data(code, tweet_ids=train_ids,
                                                            feature_type=feature_type, feature_dict=feature_dict,
                                                            fold=int(fold), **kwargs),
                                        eval_data=make_regression_data(code, tweet_ids=test_ids,
                                                            feature_type=feature_type, feature_dict=feature_dict,
                                                            fold=int(fold), **kwargs),
                                        model=model, verbose=verbose,
                                        threshold=threshold)

            results[c,0,i,:5] = scores
            results[c,0,i,5:] = n_stuff
            
    
    write_result_dict(results, '%s_%0.1f.json'%(name,threshold), codes, [name], header)
    result_dict = result_dict_from_file('%s_%0.1f.json'%(name,threshold))
    
    return results, result_dict


def run_experiments(runs, threshold=0., name='combined', name_prefix=''):
    dict_list = []
    for run in runs:
        print("Running experiment '%s'."%run[1])
        dict_list.append( (name_prefix, run_experiment(run[0], run[1], threshold=threshold, **run[2])[1]) )

    print("All experiments finished.")

    print("Creating merged result file.")
    return merge_result_dicts(dict_list, '%s%s_%0.1f.json'%(name_prefix,name,threshold))
