import numpy as np
import json

from features import make_input_features


CODES = ['aggression', 'loss', 'substance'] # sequence of codes in annotations

fold_tweets = json.load(open('data/fold_tweets.json', 'r'))
tweet_codes = json.load(open('data/tweet_codes.json', 'r'))


def merge_annotations(annotations, reconcile=True):
    # Reconciling
    if reconcile:
        for anno in annotations:
            if anno[-1]=='R': # reconcile user
                return anno[:-1]
        
    # Otherwise take the mean (there shouldn't be disagreement though)
    return np.mean([anno[:-1] for anno in annotations], axis=0)


def make_ground_truth(tweet_codes, type_='score'):
    """Create a list of ground truth items (tweet_id, collapsed_code_ground_truth_vector).
    
    :param type_: 'merge', 'full', 'score'"""
    if type_=='merge':
        return [(tweet_id, merge_annotations(tweet_codes[tweet_id])) for tweet_id in tweet_codes]
    #CAVE: In full_list items are correlated since different items might refer to the same tweet
    elif type_=='full':
        return [(tweet_id,annotation) for tweet_id in tweet_codes for annotation in tweet_codes[tweet_id]]
    elif type_=='score':
        return [(tweet_id, merge_annotations(tweet_codes[tweet_id], reconcile=False)) for tweet_id in tweet_codes]


def train_test_tweets(fold_id):
    """Utility function to get tweet IDs of training and test set for a given fold."""
    train_ids = set([tweet_id for i,fold in fold_tweets.items() for tweet_id in fold if i!=str(fold_id)])
    test_ids = set(fold_tweets[str(fold_id)])
    return train_ids, test_ids   


def make_regression_data(code, tweet_ids, feature_type=None, feature_dict=None, ground_truth_type='score', **kwargs):
    if feature_dict is None:
        feature_dict = make_input_features(feature_type, code=code, **kwargs)

    data_list = make_ground_truth({tweet_id: tweet_codes[tweet_id] for tweet_id in tweet_ids},
                                  type_=ground_truth_type)

    return [(feature_dict[d[0]], d[1][CODES.index(code)]) for d in data_list
            if d[0] in feature_dict] # NOTE: assuming that tweet IDs are all passed as strings

