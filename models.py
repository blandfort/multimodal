import numpy as np
from sklearn import svm


def random_baseline(X_train, y_train, X_eval):
    """Model that predicts the label randomly, based on the proportion of the label in the training set."""
    positive_training_fraction = np.sum(y_train)*1./len(y_train) # assuming positive class is 1 and negative 0
    eval_predictions = np.array([1 if coin<=positive_training_fraction else 0
                                 for coin in np.random.random_sample(len(X_eval))])
    
    return eval_predictions, np.repeat(eval_predictions[:,np.newaxis], 2, axis=1)


def positive_baseline(X_train, y_train, X_eval):
    """Model to always classify as positive (i.e. belonging to the respective code)."""
    # always outputting positive class
    eval_predictions = np.ones(len(X_eval))

    return eval_predictions, np.repeat(eval_predictions[:,np.newaxis], 2, axis=1)


def svm_classification(X_train, y_train, X_eval):    
    # training an SVM
    clf = svm.SVC(class_weight='balanced', probability=True, gamma='auto')
    clf.fit(X_train,y_train)

    # validation
    eval_predictions = clf.predict(X_eval)
    eval_scores = clf.predict_proba(X_eval)

    return eval_predictions, eval_scores
    

# Linguistic features require specific settings with the SVM classifier
def linguistic_svm(code, X_train, y_train, X_eval):
    if code=='aggression':
        C = .01
    elif code=='loss':
        C = .03
    elif code=='substance':
        C = .003
    else:
        C = .3
        
    # training an SVM
    clf = svm.LinearSVC(class_weight='balanced', C=C, loss='squared_hinge')
    #clf = linear_model.LogisticRegression(class_weight='balanced', C=C)
    clf.fit(X_train,y_train)

    # validation
    eval_predictions = clf.predict(X_eval)
    #eval_scores = clf.predict_proba(X_eval)

    return eval_predictions, np.array([(1-pred,pred) for pred in eval_predictions])
    

# Note: For early fusion we can use our standard SVM on the concatenated features.
def late_fusion_model(X_train, y_train, X_eval):
    """Late fusion approach where input samples are (detector1_input, detector2_input, ...)"""
    # train all the first level classifiers and compute probabilities on the dataset (to be used as input for next level classifier)
    X_train_level = []
    X_eval_level = []
    for i in range(len(X_train[0])):
        clf = svm.SVC(probability=True, class_weight='balanced', gamma='auto')
        clf.fit([sample[i] for sample in X_train],y_train)
        X_train_level.append(clf.predict_proba([sample[i] for sample in X_train]))
        X_eval_level.append(clf.predict_proba([sample[i] for sample in X_eval]))
    
    # final classifier
    clf_final = svm.SVC(probability=True, class_weight='balanced', gamma='auto')
    clf_final.fit([np.concatenate([X_train_level[i][j] for i in range(len(X_train[0]))]) for j in range(len(X_train))],y_train)
    
    eval_input = [np.concatenate([X_eval_level[i][j] for i in range(len(X_train[0]))]) for j in range(len(X_eval))]
    label_predictions = clf_final.predict(eval_input)
    score_predictions = clf_final.predict_proba(eval_input)

    return label_predictions, score_predictions
    

def late_fusion_model_with_linguistic(code, X_train, y_train, X_eval):
    """Late fusion approach where input samples are (detector1_input, detector2_input, ...)
    
    This model should be used if linguistic features are included in the late fusion.
    Uses linguistic_svm as model for the last features of the list (assuming them to be linguistic features).
    """
    # train all the first level classifiers and compute probabilities on the dataset (to be used as input for next level classifier)
    X_train_level = []
    X_eval_level = []
    for i in range(len(X_train[0])-1):
        clf = svm.SVC(probability=True, class_weight='balanced', gamma='auto')
        clf.fit([sample[i] for sample in X_train],y_train)
        X_train_level.append(clf.predict_proba([sample[i] for sample in X_train]))
        X_eval_level.append(clf.predict_proba([sample[i] for sample in X_eval]))
    # special treatment of linguistic features
    if code=='aggression':
        C = .01
    elif code=='loss':
        C = .03
    elif code=='substance':
        C = .003
    else:
        C = .3
    clf = svm.LinearSVC(class_weight='balanced', C=C, loss='squared_hinge')
    clf.fit([sample[-1] for sample in X_train], y_train)
    X_train_level.append(np.array([(1-pred,pred) for pred in clf.predict([sample[-1] for sample in X_train])]))
    X_eval_level.append(np.array([(1-pred,pred) for pred in clf.predict([sample[-1] for sample in X_eval])]))
    
    # final classifier
    clf_final = svm.SVC(probability=True, class_weight='balanced', gamma='auto')
    clf_final.fit([np.concatenate([X_train_level[i][j] for i in range(len(X_train[0]))]) for j in range(len(X_train))],y_train)
    
    eval_input = [np.concatenate([X_eval_level[i][j] for i in range(len(X_train[0]))]) for j in range(len(X_eval))]
    label_predictions = clf_final.predict(eval_input)
    score_predictions = clf_final.predict_proba(eval_input)

    return label_predictions, score_predictions
