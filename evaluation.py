import numpy as np


# EVALUATION METRICS

def result_matrix(predictions, ground_truth, num_classes=2): # first dimension ground truth, second dimension prediction
    """Compute a confusion matrix from a list of predictions, given a list of ground truth labels.
    
    NOTE: Assuming that class labels (for prediction and ground_truth) are integers from 0 to num_classes-1"""
    matrix = np.zeros((num_classes,num_classes))
    for i in range(num_classes):
        #total_for_label = len([d for d in ground_truth if d==i])
        # More efficient version: len(np.where(np.array(ground_truth)==i)[0])
        
        for j in range(num_classes):
            correct_for_label = sum(np.array([predictions[k] for k,d in enumerate(ground_truth) if d==i])==j)
            matrix[i,j] = correct_for_label #*100./total_for_label
            
    return matrix


def precision_recall_F1(predictions, ground_truth):
    res = result_matrix(predictions, ground_truth)
    TN = res[0,0]
    FN = res[1,0]
    TP = res[1,1]
    FP = res[0,1]
    precision = TP*1./(TP+FP)
    recall = TP*1./(TP+FN)
    F1 = 2*precision*recall*1./(precision+recall)
    return precision, recall, F1


# Function to compute the average precision
# Note that we only consider those k where a new relevant document is added to the list.
average_precision_fct = lambda sequence:\
    sum([precision_fct(sequence,k+1) for k in range(len(sequence)) if sequence[k]])*1./sum(sequence)
    
# Function to compute precision at k
precision_fct = lambda sequence, k: sum(sequence[:k])*1./k


def compute_performance(eval_pred, eval_scores, eval_gt):
    # assuming that labels are 0 or 1 where 1 is the positive class
    n_pos_out = np.sum(eval_pred)
    n_pos_true = np.sum(eval_gt)
        
    precision,recall,F1 = precision_recall_F1(eval_pred, eval_gt)
    accuracy = np.sum(eval_pred==eval_gt)*1./len(eval_pred)
    # AP
    ranked_labels = np.array(sorted([(eval_scores[j,1],eval_gt[j]) for j in range(len(eval_gt))],
                                    key=lambda x: -x[0]))[:,1]
    AP = average_precision_fct(ranked_labels)

    return (precision, recall, F1, accuracy, AP), ('precision', 'recall', 'F1', 'accuracy', 'AP'), (n_pos_out, n_pos_true)


