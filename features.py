import pickle
import numpy as np
import csv
import json


feature_dir = '/home/blandfort/code/git/multimodal/data/features/'

linguistic_feature_dir = feature_dir+'text_svm/'
cnn_feature_dir = feature_dir+'text_cnn/'
global_feature_dir = feature_dir+'global_visual/'
local_feature_dir = feature_dir+'local_visual/'

CONCEPT_LIST = ['lean-container', 'tattoo', 'marijuana (raw)', 'hand gesture', 'person', 'money',
                      'joint / blunt / cigarette', 'handgun', 'long gun']



# TEXT FEATURES


def svm_features(code, train_or_test='train', fold=0):
    dat = pickle.load(open(linguistic_feature_dir+'cv/%s/%d/'%(code,fold+1)+train_or_test+'_features.p', 'rb'),
                      encoding='latin1') # setting encoding because pickle file was written in python 2
    
    # corresponding tweet IDs
    info_file = ''.join([line for line in open(linguistic_feature_dir+'img_data/%d/'%(fold+1)+train_or_test+'.csv', 'r')])
    tweet_ids = [part.split(',')[0] for part in info_file.split('http://twitter.com/statuses/')][1:]

    # putting stuff together into a pretty dictionary
    feature_dict = {}
    #print(dat[0].shape) #-> 374, 1300 for first split
    for ix,tweet_id in enumerate(tweet_ids):
        feature_dict[tweet_id] = np.array(dat[0][ix].todense())[0]
        
    return feature_dict


# CNN-word and CNN-char
def get_text_features(code, base_name='final_test_2018_04_01', fold=0, model='cnn-word', read_ix=2): # positive class second
    # 0 true labels
    # 1 predicted labels
    # 2 prediction probabilities
    # 3 tweet_representations
    if model=='cnn-word':
        #path = 'results/acmmm_paper/%s/cross_validation/fold%d/run/'%(code,fold)
        path = '%s/cross_validation/fold%d/run/'%(code,fold+1)
    else: # cnn-char
        #path = 'results/acmmm_paper/%s/cross_validation/fold%d/run_char2/'%(code,fold)
        path = '%s/cross_validation/fold%d/run_char2/'%(code,fold+1)
    dat = pickle.load(open(cnn_feature_dir+path+base_name+'_predictions.p', 'rb'), encoding='latin1')
    # Format of pickled file: true labels, predicted labels, prediction probabilities, tweet_representations, class_names
    # Note: We use encoding='latin1' because the pickle file was written in python 2
    
    # corresponding tweet IDs
    tweet_ids = [line.strip().split('<:>')[-1]
                 for line in open(cnn_feature_dir+path+base_name+'.txt', 'r')]

    # putting stuff together into a pretty dictionary
    feature_dict = {}
    for ix,tweet_id in enumerate(tweet_ids):
        if read_ix==2:
            if dat[-1][1]=='other':
                feature_dict[tweet_id] = dat[2][ix,::-1] # 2 for prediction probabilities, 3 for tweet representations
                # -1 to have positive score last
            else:
                feature_dict[tweet_id] = dat[2][ix]
        elif read_ix==1: # labels
            if dat[-1][1]=='other':
                feature_dict[tweet_id] = 1-dat[1][ix]
            else:
                feature_dict[tweet_id] = dat[1][ix]
        else:
            feature_dict[tweet_id] = dat[read_ix][ix]
            #print dat[read_ix]
        
    return feature_dict


def text_fold_features(code,fold=0,date='2018_04_01', model='cnn-word', read_ix=2):
    """Use this function to load textual features as dictionary {tweet_id: features}."""
    id_dict = {}
    if model=='svm':
        id_dict.update(svm_features(code, 'test', fold=fold))
        id_dict.update(svm_features(code, 'train', fold=fold))
    else:
        id_dict.update(get_text_features(code, 'final_test_'+date, fold=fold, model=model, read_ix=read_ix))
        id_dict.update(get_text_features(code, 'final_tr_val_'+date, fold=fold, model=model, read_ix=read_ix))
        id_dict.update(get_text_features(code, 'final_tr_tr_'+date, fold=fold, model=model, read_ix=read_ix))
    
    # using image names as keys instead
    #_, tweet_data = code_tweets(allowed_users=None)
    #feature_dict = {tweet_data[tweet_id][0].split('/')[-1]: id_dict[tweet_id] for tweet_id in id_dict}

    #return feature_dict
    return id_dict



# VISUAL FEATURES

def annotation_bboxes(annotation, header, gold, reconcile=True):
    if not annotation[header.index('bounding_boxes')]:
        bboxes = {}
    else:
        try:
            bboxes = [bbox for bbox in json.loads(annotation[header.index('bounding_boxes')])]
        except:
            bboxes = [bbox for bbox in eval(annotation[header.index('bounding_boxes')])]
        # restructure by concept
        bboxes = {concept: [concept_bboxes for concept_bboxes in bboxes if concept_bboxes['label']==concept]
                 for concept in set([bbox['label'] for bbox in bboxes])}

    # use gold standard to overwrite certain parts
    if reconcile:
        tweet_id = annotation[header.index('tweet_id')]
        if '.' in tweet_id:
            tweet_id = tweet_id.split('.')[0]
        tweet_id = int(tweet_id)
        if tweet_id in gold:
            for concept in gold[tweet_id]:
                bboxes[concept] = gold[tweet_id][concept]
    return bboxes


def concept_bboxes(annotation, concept):
    if not annotation:
        return []
    return [bbox for bbox in json.loads(annotation) if bbox['label']==concept]


def local_vector(bboxes, excluding_concepts=[], concepts=None):
    if concepts is None:
        concepts = CONCEPT_LIST # use global list by default
    present_concepts = {concept: len(bboxes[concept]) for concept in bboxes}
    return [present_concepts[concept] if (concept in present_concepts and concept not in excluding_concepts) else 0
            for concept in concepts]
            

def score_vector(bboxes, excluding_concepts=[], concepts=None):
    if concepts is None:
        concepts = CONCEPT_LIST # use global list by default
    concept_scores = {concept: sum([bbox['score'] for bbox in bboxes[concept]]) for concept in bboxes}
    return [concept_scores[concept] if (concept in concept_scores and concept not in excluding_concepts) else 0
            for concept in concepts]


def local_detection_dict(fold, detection_threshold, **kwargs):
    items = {}
    with open(local_feature_dir+'fold_%d/fold_%d_%0.1f.csv'%(fold,fold,detection_threshold), 'r') as f:
        csvread = csv.reader(f, delimiter=',')
        for i,row in enumerate(csvread):
            if i==0:
                header = row
            else:
                tweet_id = row[header.index('tweet_id')]
                if '.' in tweet_id:
                    tweet_id = tweet_id.split('.')[0]
                if tweet_id in items.keys():
                    items[tweet_id] += [row]
                else:
                    items[tweet_id] = [row]

    tweet_bboxes = {}

    # convert the format
    for tweet_id,res in items.items():
        annotations = []
        for r in res:
            annotations.append(annotation_bboxes(r, header, []))

        tweet_bboxes[tweet_id] = annotations #random.choice(annotations)
    
    return tweet_bboxes
    

def local_features(tweet_dict, use_scores=False, concept_list=None):

    feature_dict = {}
    for tweet_id,bboxes in tweet_dict.items():

        feature_dict[tweet_id] = []
        
        for ann in bboxes:
            if use_scores:
                feature_dict[tweet_id].append(score_vector(ann, concepts=concept_list))
            else:
                feature_dict[tweet_id].append(local_vector(ann, concepts=concept_list))

        feature_dict[tweet_id] = np.mean(feature_dict[tweet_id], axis=0)

    return feature_dict


def inception_features(tweet_image_dict_path='data/tweet_images.json'):
    """load inception features from files"""
    # util dictionary to map from tweet IDs to image names
    tweet_images = {tweet_id: img_url.split('/')[-1]
                for tweet_id,img_url in json.load(open(tweet_image_dict_path, 'r')).items()}
    
    features = np.load(global_feature_dir+'image_features.npy')
    img_paths = [line.strip() for line in open(global_feature_dir+'image_list.txt', 'r')]

    img_fts = {img_paths[i].split('/')[-1]: features[i] for i in range(len(features))}
    ft_dict = {}
    for tweet_id in tweet_images:
        if tweet_images[tweet_id] in img_fts:
            ft_dict[tweet_id] = img_fts[tweet_images[tweet_id]]
    return ft_dict
    
    
# FUSION FEATURES

def early_fuse_dicts(*dicts):
    feature_dict = {}
    
    for key in dicts[0].keys():
        if all([key in d for d in dicts]):
            feature_dict[key] = np.concatenate([d[key] for d in dicts])
            
    return feature_dict
    

def late_fuse_dicts(*dicts):
    feature_dict = {}
    for key in dicts[0].keys():
        if all([key in d for d in dicts]):
            feature_dict[key] = [dict_[key] for dict_ in dicts]
    return feature_dict
    
    
    
# CONVENIENCE FUNCTION


def make_input_features(feature_type, **kwargs):

    if feature_type=='global_visual':
        feature_dict = inception_features()
        
    elif feature_type=='local_visual':
        feature_dict = local_features(local_detection_dict(**kwargs)) # need to pass fold and detection_threshold
    
    elif feature_type=='linguistic':
        feature_dict = text_fold_features(model='svm', **kwargs) # need to pass code and fold
        
    elif feature_type=='cnn-word':
        feature_dict = text_fold_features(model='cnn-word', read_ix=3, **kwargs) # need to pass code and fold

    elif feature_type=='cnn-char':
        feature_dict = text_fold_features(model='cnn-char', read_ix=3, **kwargs) # need to pass code and fold
        
    else: # fusion features - use "X_Y" where X is early/late and Y is text/visual/all
        dicts = []
        
        if feature_type.endswith('visual') or feature_type.endswith('all'):
            dicts.append(inception_features())
            dicts.append(local_features(local_detection_dict(detection_threshold=.1, **kwargs))) # need to pass fold
            dicts.append(local_features(local_detection_dict(detection_threshold=.5, **kwargs))) # need to pass fold
        if feature_type.endswith('text') or feature_type.endswith('all'):
            dicts.append(text_fold_features(model='cnn-word', read_ix=3, **kwargs)) # need to pass fold
            dicts.append(text_fold_features(model='cnn-char', read_ix=3, **kwargs)) # need to pass fold
            dicts.append(text_fold_features(model='svm', **kwargs)) # need to pass fold
            # Note: linguistic features are added last.
            # This is important to note for late fusion since these features require a special SVM for detection.
            
        if feature_type.startswith('early'):
            feature_dict = early_fuse_dicts(*dicts)
        else:
            feature_dict = late_fuse_dicts(*dicts)
    
    return feature_dict

