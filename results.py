import scipy
import numpy as np
import json
from tabulate import tabulate


RESULT_DIR = 'results/'


#https://stackoverflow.com/questions/15033511/compute-a-confidence-interval-from-sample-data
def confidence_interval(data, confidence=0.95):
    return scipy.stats.t.interval(confidence, len(data)-1, loc=np.mean(data), scale=scipy.stats.sem(data))
   

def write_result_dict(results, result_filename, codes, version_names, measure_names):
    result_dict = {code:
                  {version_name:
                   {measure: list(results[i,j][:,k]) for k,measure in enumerate(measure_names)}
                   for j,version_name in enumerate(version_names)}
                  for i,code in enumerate(codes)}

    with open(RESULT_DIR+result_filename, 'w') as f:
        f.write(json.dumps(result_dict))

        
def result_dict_from_file(filename):
    return json.loads([line for line in open(RESULT_DIR+filename, 'rb')][0])
        
        
def show_results(result_dict, measure_names, floatfmt=".2f", include_confidence=True):
    
    for code in result_dict.keys():
        print('\n\n%s\n'%code)
        
        table_data = []
        for version_name in sorted(result_dict[code]):
            table_row = [version_name]
            
            for measure_name in measure_names:
                if measure_name in result_dict[code][version_name]:
                    #table_row += [np.mean(result_dict[code][version_name][measure_name])]
                    numbers = result_dict[code][version_name][measure_name]
                    if include_confidence:
                        table_row += ['%0.2f (%0.2f-%0.2f)'%(np.mean(numbers), confidence_interval(numbers)[0], confidence_interval(numbers)[1])]
                    else:
                        table_row += [np.mean(numbers)]
                else:
                    table_row += ['-', '-']
            table_data.append(table_row)

        print(tabulate(table_data, headers=[]+measure_names, floatfmt=floatfmt))

        
def merge_result_dicts(in_list, result_filename):
    """Take a list (version_name_prefix, result_dict) and merge all results
    from the result dicts into a single file."""
    result_dict = {}
    
    for prefix,current_dict in in_list:
        #current_dict = result_dict_from_file(filename)
        for code in current_dict:
            if code not in result_dict:
                result_dict[code] = {}
            for version_name in current_dict[code]:
                result_dict[code][prefix+version_name] = current_dict[code][version_name]
                
    with open(RESULT_DIR+result_filename, 'w') as f:
        f.write(json.dumps(result_dict))
        
    return result_dict
